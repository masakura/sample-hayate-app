﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using SampleApp.Core;

namespace SampleHayateApp.Pages
{
    public class IndexModel : PageModel
    {
        public IndexModel(WelcomeMessage message)
        {
            Message = message.Text;
        }

        public string Message { get; }

        public void OnGet()
        {
        }
    }
}